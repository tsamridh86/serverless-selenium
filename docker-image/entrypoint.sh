#!/bin/bash

java -jar selenium-server-standalone-3.141.59.jar -role standalone -timeout 300 &
pID=`ps -ef | grep "java -jar selenium" | grep -v "grep" | awk '{print $2}'`
while true
do
    sleep 300
    session=$(wget -qO- http://localhost:4444/wd/hub/sessions | jq -r ".value")
    if [[ $session = [] ]]
        then
        break
    fi
done
kill -9 $pID
exit
        