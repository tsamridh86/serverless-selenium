from copy import deepcopy

serviceTemplate = {
    "apiVersion" : "v1",
    "kind" : "Service",
    "metadata" : {
        "name" : "variable1",
        "labels":{
            "app" : "variable2",
        },
    },
    "spec" : {
        "type" : "ClusterIP",
        "ports" : [
            {
                "port" : 4444,
            }
        ] ,
        "selector" : {
            "app" : "variable2"
        },
    }
}

jobTemplate = {
    "apiVersion": "batch/v1",
    "kind": "Job",
    "metadata":{
        "name": "jobName",
        "labels":{
            "app" : "serviceToExpose",
        },
    },
    "spec":{
    "template":{},
    },
}

deploymentTemplate = {
    "apiVersion": "apps/v1",
    "kind": "Deployment",
    "metadata":{
        "name": "deploymentName",
        "labels":{
            "app": "exposedServiceName",
        },
    },
    "spec":{
        "template":{},
        "replicas": 3,
        "selector":{
            "matchLabels":{
                "app": "exposedServiceName"
            }
        }
    }
}


podTemplate = {
    "metadata":{
        "name": "podName",
        "labels":{
            "app": "exposedServiceName",
        },
    },
    "spec":{
        "containers":
        [
            {
                "name": "containerName",
                "image": "imageName",
            },
        ],
    },
}

def podGenerator(podName, exposedServiceName, containerName, imageName):
    newPod = deepcopy(podTemplate)
    newPod['metadata']['name'] = podName
    newPod['metadata']['labels']['app'] = exposedServiceName
    newPod['spec']['containers'][0]['name'] = containerName
    newPod['spec']['containers'][0]['image'] = imageName
    return newPod

# this function is un-used so far, but can come in handy
def getPodDefintion(podName, exposedServiceName, containerName, imageName):
    return (podGenerator(podName, exposedServiceName, containerName, imageName))

def getServiceDefinition(serviceName, labelToExpose):
    newService = deepcopy(serviceTemplate)
    newService['metadata']['name'] = serviceName
    newService['metadata']['labels']['app'] = labelToExpose
    newService['spec']['selector']['app'] = labelToExpose
    return (newService)

def getJobDefinition(jobName,exposedServiceName, podName, containerName, imageName):
    newJob = deepcopy(jobTemplate)
    newJob['metadata']['name'] = jobName
    newJob['metadata']['labels']['app'] = exposedServiceName
    newJob['spec']['template'] = podGenerator(podName, exposedServiceName, containerName, imageName)
    newJob['spec']['template']['spec']['restartPolicy'] = "Never"
    return (newJob)

def getDeploymentDefinition(deploymentName, exposedServiceName, podName, containerName, imageName, initReplicas=3):
    newDeployment = deepcopy(deploymentTemplate)
    newDeployment['metadata']['name'] = deploymentName
    newDeployment['metadata']['labels']['app'] = exposedServiceName
    newDeployment['spec']['template']= podGenerator(podName, exposedServiceName, containerName, imageName)
    newDeployment['spec']['replicas'] = initReplicas
    return (newDeployment)

label = "simple-serverless-selenium"
podName = "selenium-standalone"
containerName = "serverless-selenium-chrome-standalone"
imageName = "docker.io/selenium/standalone-chrome"


# print(getServiceDefinition("serverless-selenium-service", label ))
# print(getJobDefinition("simple-selenium-job", label, podName, containerName, imageName))
# print(getDeploymentDefinition("simple-selenium-deployment",label,podName, containerName,imageName,3))