from json import load
from localCommandExecutor import LocalCommandExecutor
import yamlGenerator
from CommandExecutorInterface import CommandExecutorInterface
from UtilInterface import UtilInterface

class ImageUtils(UtilInterface):

    def __init__(self, executor):
        self.executor = executor
        if not isinstance(self.executor, CommandExecutorInterface):
            raise Exception("The executor is not compatible with the interface")

    def findVersionAndBrowser(self, capabilityString):
        parsedString = capabilityString.replace("{","").replace("}","").replace(",","=").split("=")
        parsedString = list(map(lambda x: x.strip(),parsedString)) 
        browserName = parsedString[parsedString.index("browserName") + 1 ].split(",")[0]
        browserVersion = parsedString[parsedString.index("version") + 1 ].split(",")[0].split(".")[0]
        return browserVersion, browserName

    def getImageName(self,browserName, browserVersion):
        with open("availableImages.json", "r") as imgRepo:
            imageMetaDataRepository = load(imgRepo)
            try:
                return imageMetaDataRepository[browserName][browserVersion]
            except KeyError:
                return imageMetaDataRepository["chrome"]["76"]

    
    def getRequiredImage(self, desiredCapability):
        version, browser = self.findVersionAndBrowser(desiredCapability)
        return self.getImageName(browser,version)


    def createService(self):
        serviceName = self.executor.getUniqueName("service-")
        exposedLabel = self.executor.getUniqueName("label-")
        serviceDefinition = yamlGenerator.getServiceDefinition(serviceName, exposedLabel)
        self.executor.createService(serviceDefinition)
        return serviceName, exposedLabel

    def createJob(self, exposedLabel, imageName):
        jobName = self.executor.getUniqueName("job-")
        podName = self.executor.getUniqueName("pod-")
        containerName = self.executor.getUniqueName("container-")
        jobDefinition = yamlGenerator.getJobDefinition(jobName,exposedLabel,podName,containerName,imageName)
        self.executor.createJob(jobDefinition)
        return jobName

    def updateIngress(self, serviceName):
        self.executor.updateIngress(serviceName)


def startServices(executor):
    commandExecutor = executor
    commandExecutor.runKubectlApply("controllers.yaml")
    commandExecutor.runKubectlApply("cloud-generic.yml")
    ipAddress = commandExecutor.getServiceExternalIP("ingress-nginx")
    if ipAddress == None:
        raise EnvironmentError("could not get IP address for the services")
    return ipAddress