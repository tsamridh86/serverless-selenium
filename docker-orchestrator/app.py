from flask import Flask, redirect, url_for, render_template, request
from subprocess import PIPE, run
import re
import time
import imageUtils
from localCommandExecutor import LocalCommandExecutor

# Initialize the Flask application
app = Flask(__name__)
mainServiceIP = None
allPorts = {}

@app.route('/wd/hub',methods=['GET','POST'])
def connectToHub():
    global mainServiceIP
    desiredCaps = request.form['desiredCaps']
    executor = LocalCommandExecutor()
    utils = imageUtils.ImageUtils(executor)
    imageName = utils.getRequiredImage(desiredCaps)
    serviceName, exposedLabel = utils.createService()
    utils.createJob(exposedLabel,imageName)
    utils.updateIngress(serviceName)
    return redirect("http://"+mainServiceIP+"/"+serviceName,code=302)


def runStartupScript():
    global mainServiceIP
    executor = LocalCommandExecutor()
    mainServiceIP = imageUtils.startServices(executor)
    print(mainServiceIP)

if __name__ == '__main__':
    runStartupScript()
    print("service is up, ready to accept incoming connections")
    app.run(debug = True)

