from abc import ABCMeta, abstractmethod

class UtilInterface(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self, executor):
        pass

    @abstractmethod
    def getRequiredImage(self, desiredCaps):
        pass

    @abstractmethod
    def createService(self):
        pass

    @abstractmethod
    def createJob(self, exposedLabel, imageName):
        pass

    @abstractmethod
    def updateIngress(self, serviceName):
        pass
