from subprocess import run, getoutput, PIPE
from datetime import datetime
from yaml import dump
import yamlGenerator
from re import match
from time import sleep
from string import Template
from CommandExecutorInterface import CommandExecutorInterface

class LocalCommandExecutor(CommandExecutorInterface):

    def __init__(self):
        self.__uniqueValue = self.__getCurrentTimeStamp()
        self.IPAddrPattern = r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$"

    def __getCurrentTimeStamp(self):
        return str(int(datetime.now().timestamp()))

    def getUniqueName(self, prepend="", append=""):
        return f'{prepend}{self.__uniqueValue}{append}'

    def runKubectlApply(self, fileName):
        return run(["kubectl","apply","-f",fileName])

    def createYamlFile(self, fileType, definition):
        newDefinitionFileName = self.getUniqueName(fileType, ".yaml")
        with open(newDefinitionFileName, "w") as yamlFile:
            dump(definition, yamlFile)
        return newDefinitionFileName

    def createService(self, serviceDefinitionYaml):
        newServiceDefitionFileName = self.createYamlFile("service", serviceDefinitionYaml)
        self.runKubectlApply(newServiceDefitionFileName)
    
    def createJob(self, jobDefinitonYaml):
        newJobDefinitionFileName = self.createYamlFile("job", jobDefinitonYaml)
        self.runKubectlApply(newJobDefinitionFileName)
    
    def getServiceExternalIP(self, serviceName):
        ipAddress = None
        for _ in range(50):
            output = getoutput("kubectl get svc -n "+serviceName+" --no-headers=True | awk '{print $4}'")
            isIP = match(self.IPAddrPattern,output) 
            if isIP:
                ipAddress = isIP.group()
                break
            sleep(10)
        return ipAddress

    def updateIngress(self, serviceName):
        newLine = "\n"
        space = " "
        insertCommand = f'{newLine}{space*6}- path: /{serviceName}(/|$)(.*){newLine}{space*8}backend:{newLine}{space*10}serviceName: {serviceName}{newLine}{space*10}servicePort: 4444{newLine}'
        with open("ingress.yaml", "a+") as ingressFile:
            ingressFile.write(insertCommand)
        self.runKubectlApply("ingress.yaml")
