from abc import ABCMeta, abstractmethod

class CommandExecutorInterface(metaclass=ABCMeta):
    
    @abstractmethod
    def getUniqueName(self, prepend, append):
        pass

    @abstractmethod
    def createService(self, serviceDefinitionYaml):
        pass

    @abstractmethod
    def runKubectlApply(self, fileName):
        pass

    @abstractmethod
    def getServiceExternalIP(self, serviceName):
        pass

    @abstractmethod
    def updateIngress(self, serviceName):
        pass

    @abstractmethod
    def createJob(self, jobDefinitonYaml):
        pass
