This project is an attempt to create a serverless version of selenium.

Current issues with selenium :
    1. Chrome auto-updates itself and the driver does not, although the latest version of the driver is always available, we need to download it everytime.
        a. This can be bypassed if there is a script written for it, which checks on a regular interval, if there are any new versions of the chrome driver that is available.
    2. Selenium grid need to be all of the time.
        a. If the grid master goes down, then we are unable to contact the nodes and perform tasks on them.
         

Proposed Solution in  a 10,000 ft overview:

Create a serverless version of selenium, that only executes when required. It would auto-update itself on demand, and the chromedriver and chrome would always be in sync.


More details:
1.  A simple docker container image will be created, this would contain a standalone selenium server, chromedriver and chrome installed.
    This container will be scripted in such a way that the above software are in thier latest, correct versions.
    If the no client connections are detected, then the container will auto-self-destruct itself.
    A regression pack can be created later to auto-test it and would not create the image and fall back to an older version if the test fails.
    
    
2. A simple redirection server made in flask with three duties:
    a. Create the server mentioned above
    b. Redirect the requesting client to the newly created selenium server.
    c. Kill the server if the client requests for closure.
    
3. A new jar file, with DockerDriver that can abstract the above issues and has the same usability as the standard selenium driver.
    This jar file is created as an uber-jar currently, one may directly start using the DriverFactory and start writing test cases.
    
