package com.example.seleniumclient;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;


public class DriverFactory {

	public static RemoteWebDriver getRemoteDriver() throws IOException {
		ChromeOptions options = new ChromeOptions();
		options.setCapability("version", 76);
		options.addArguments("--no-sandbox");
		options.addArguments("--headless");
		return new DockerDriver("http://localhost:5000/wd/hub",options);
		// return new RemoteWebDriver( new URL("http://localhost:4444/wd/hub"),capabilities);
		// return new RemoteWebDriver( new URL("http://34.93.67.153:4444/wd/hub"),capabilities);
	}
	public static void main(String[] args) throws IOException {

		RemoteWebDriver driver = getRemoteDriver();
		// driver.get("https://www.google.com");
		// System.out.println(driver.getCurrentUrl());
		// File src  = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// FileUtils.copyFile(src, new File("google-sami.png"));
		driver.close();
	}

	
}