package com.example.seleniumclient;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import okhttp3.Call;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DockerDriver extends RemoteWebDriver {

    public URL containerCreatorURL;
    public static URL createdURL;

    public DockerDriver(String url, MutableCapabilities capabilities) throws IOException {
        super(getBrowser(url, capabilities),capabilities);
        this.containerCreatorURL = new URL(url);
    }

    private static URL getBrowser(String providerURL, MutableCapabilities capabilities) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                                                .followRedirects(false)
                                                .connectTimeout(15, TimeUnit.SECONDS)
                                                .readTimeout(15, TimeUnit.SECONDS)
                                                .writeTimeout(15, TimeUnit.SECONDS)
                                                .build();

        RequestBody requestBody = new MultipartBody.Builder()
                                                   .setType(MultipartBody.FORM)
                                                   .addFormDataPart("desiredCaps", capabilities.toJson().toString())
                                                   .build();

        System.out.println(capabilities.toJson().toString());

        Request request = new Request.Builder().url(providerURL).post(requestBody).build();
        Call call = client.newCall(request);
        Response response = call.execute();
        
        createdURL = new URL(response.header("Location"));
        String finalURL =  createdURL.toExternalForm()+ "wd/hub";
        System.out.println("final URL : "+ finalURL);
        return new URL(finalURL);
    }

    @Override
    public void close(){
        OkHttpClient client = new OkHttpClient();
        String surrenderURL = containerCreatorURL.getProtocol() + "://" + containerCreatorURL.getAuthority()+"/surrender/"+createdURL.getPort();
        Request request = new Request.Builder().url(surrenderURL).build();
        Call call = client.newCall(request);
        try {
            call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   
}